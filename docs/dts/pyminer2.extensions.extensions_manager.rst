pyminer2.extensions.extensions\_manager package
===============================================

Submodules
----------

pyminer2.extensions.extensions\_manager.ExtensionLoader module
--------------------------------------------------------------

.. automodule:: pyminer2.extensions.extensions_manager.ExtensionLoader
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.extensions\_manager.UIInserter module
---------------------------------------------------------

.. automodule:: pyminer2.extensions.extensions_manager.UIInserter
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.extensions\_manager.log module
--------------------------------------------------

.. automodule:: pyminer2.extensions.extensions_manager.log
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.extensions\_manager.manager module
------------------------------------------------------

.. automodule:: pyminer2.extensions.extensions_manager.manager
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.extensions.extensions_manager
   :members:
   :undoc-members:
   :show-inheritance:
