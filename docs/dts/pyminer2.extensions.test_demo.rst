pyminer2.extensions.test\_demo package
======================================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   pyminer2.extensions.test_demo.extensions

Submodules
----------

pyminer2.extensions.test\_demo.extensionlib module
--------------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.extensionlib
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.extensions.test\_demo.mainform module
----------------------------------------------

.. automodule:: pyminer2.extensions.test_demo.mainform
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.extensions.test_demo
   :members:
   :undoc-members:
   :show-inheritance:
