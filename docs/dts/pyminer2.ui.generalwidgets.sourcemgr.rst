pyminer2.ui.generalwidgets.sourcemgr package
============================================

Submodules
----------

pyminer2.ui.generalwidgets.sourcemgr.iconutils module
-----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.sourcemgr.iconutils
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.sourcemgr
   :members:
   :undoc-members:
   :show-inheritance:
