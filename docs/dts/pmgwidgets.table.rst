pmgwidgets.table package
========================

Submodules
----------

pmgwidgets.table.tableviews module
----------------------------------

.. automodule:: pmgwidgets.table.tableviews
   :members:
   :undoc-members:
   :show-inheritance:

pmgwidgets.table.tablewidgets module
------------------------------------

.. automodule:: pmgwidgets.table.tablewidgets
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pmgwidgets.table
   :members:
   :undoc-members:
   :show-inheritance:
