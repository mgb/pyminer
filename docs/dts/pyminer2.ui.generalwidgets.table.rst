pyminer2.ui.generalwidgets.table package
========================================

Submodules
----------

pyminer2.ui.generalwidgets.table.tableviews module
--------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.table.tableviews
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.table.tablewidgets module
----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.table.tablewidgets
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.table
   :members:
   :undoc-members:
   :show-inheritance:
