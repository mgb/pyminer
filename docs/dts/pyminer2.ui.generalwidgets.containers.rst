pyminer2.ui.generalwidgets.containers package
=============================================

Submodules
----------

pyminer2.ui.generalwidgets.containers.PMTab module
--------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.containers.PMTab
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.containers.flowarea module
-----------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.containers.flowarea
   :members:
   :undoc-members:
   :show-inheritance:

pyminer2.ui.generalwidgets.containers.pmscrollarea module
---------------------------------------------------------

.. automodule:: pyminer2.ui.generalwidgets.containers.pmscrollarea
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: pyminer2.ui.generalwidgets.containers
   :members:
   :undoc-members:
   :show-inheritance:
